// @generated: @expo/next-adapter@2.0.0-beta.10
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Layout from '../components/Layout';

export default function App() {
  return (
    <Layout>
      <View style={styles.container}>
        <Text style={styles.text}>Welcome to Expo + Next.js 👋</Text>
      </View>
    </Layout>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontSize: 16,
  },
});

import Link from 'next/link';
import { View, Text } from 'react-native';

const Header = () => (
  <View>
    <Link href="/">
      <Text>Home</Text>
    </Link>
    <Link href="/about">
      <Text>About</Text>
    </Link>
  </View>
);

export default Header;

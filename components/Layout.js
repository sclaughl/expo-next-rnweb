import Header from './Header';
import { View } from 'react-native';

const Layout = props => (
  <View>
    <Header />
    {props.children}
  </View>
);

export default Layout;
